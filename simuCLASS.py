import pdb
import numpy as np
import sys
import os
#import pyfits as fits
import ConfigParser
import subprocess

from skymodel.skymodel import runSkyModel
from simulatedata.simdata import runSimulateData
from imager.imager import runNWImager, runCASAClean, runWSClean
from thumbnailer.thumbnailer import makeThumbnails

config = ConfigParser.ConfigParser()
config.read(sys.argv[1])

# ToDo: a bit of arithmetic on the ra and dec to give to runskymodel (as new config parameter)

if config.getboolean('pipeline', 'doskymodel'):
  runSkyModel(config)

if config.getboolean('pipeline', 'dosimdata'):
  runSimulateData(config, sys.argv[1])

if config.getboolean('pipeline', 'doimagedata'):
  if config.get('imager', 'type') == 'casaclean':
    runCASAClean(config, sys.argv[1])
  elif config.get('imager', 'type') == 'nwimager':
    runNWImager(config, sys.argv[1])
  elif config.get('imager', 'type') == 'wsclean':
    runWSClean(config)
    if config.getboolean('imager', 'dopostagestamps'):
      makeThumbnails(config)
  else:
    'You picked an unknown imager!'
