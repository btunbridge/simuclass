import numpy as np
import os
import time
import pdb
import ConfigParser

from emerlin_antenna import *
from vla_antenna import *

def runSimulateData(config, parameter_filename):
  
  run_dir = os.getcwd()
  
  cmd = 'casapy --nogui --log2term -c {0}/simulatedata/simdata.py {0}/{1}'.format(run_dir, parameter_filename)
  print(cmd)
  os.chdir(config.get('pipeline', 'data_path'))
  os.system(cmd)


if __name__=='__main__':

  config = ConfigParser.ConfigParser()
  config.read(sys.argv[-1])
  #config.read('example.ini')
  n_ifs = config.getint('observation', 'n_IFs')
  bw = config.getfloat('observation', 'total_bandwidth')
  base_freq = config.getfloat('observation', 'lowest_frequency')
  n_chan = config.getint('observation', 'n_channels')
  msname = config.get('pipeline', 'project_name')+'.ms'
  msname = config.get('pipeline', 'data_path')+msname
  imagename = msname+'.image'
  
  channel_width = bw / (n_chan*n_ifs)
  if_width = bw / n_ifs
  
  '''
  from simulatedata.fixImage import fixImage
  
  fixImage(datafile=config.get('field', 'fitsname'),
  				 centre_freq=base_freq,
  				 bandwidth=bw*n_chan*n_ifs,
  				 n_channels=n_chan*n_ifs,
  				 pixel_scale=config.getfloat('skymodel', 'pixel_scale'),
  				 Ra_central=config.get('field', 'field_ra'),
  				 Dec_central=config.get('field', 'field_dec'),
  				 input_casa_image=imagename)
  '''
  fitsimage = config.get('pipeline', 'data_path')+config.get('field', 'fitsname')
  importfits(fitsimage=fitsimage, imagename=imagename, overwrite=True)
  
  '''
  from vla_antenna import *
  sm.setspwindow(spwname = 'VLA-spw',
                 freq = '1.4GHz',
                 deltafreq = '3MHz',
                 freqresolution = '3MHz',
                 nchannels = 14,
                 stokes = 'LL RR')

  posvla = me.observatory('vla')
  sm.setconfig(telescopename = 'VLA-B',
               x = vla_xx,
               y = vla_yy,
               z = vla_zz,
               dishdiameter = diam.tolist(),
               mount = 'alt-az',
               coordsystem = 'local',
               referencelocation=posvla)
             
  sm.setfeed(mode='perfect R L')
  sm.setfield(sourcename = 'field1',
              sourcedirection = 'J2000 0h0m0.0s +90d0m0.0s')
  obs_date = '2011/03/21'
  ref_time = me.epoch('IAT',obs_date)
  sm.settimes(integrationtime = '5s',
              usehourangle = True,
              referencetime=ref_time)
            
  sm.observe('field1', 'VLA-spw',
             starttime = '0s', stoptime = '7200s')
  
  '''
  if config.getboolean('pipeline', 'domakecoverage'):
    sm.open(msname)
    for i in np.arange(1,n_ifs+1):
      fr = base_freq + if_width*(i-1)
      print(fr, bw, i)
      sm.setspwindow(spwname = 'IF'+str(i),
                     freq = str(fr)+'Hz', # starting frequency
                     deltafreq = str(channel_width)+'Hz', # increment per chan
                     freqresolution = str(channel_width)+'kHz', # width per chan
                     nchannels = n_chan,
                     stokes = 'LL RR')

    if config.get('observation', 'telescope')=='e-merlin':
      observatory = 'e-MERLIN'
      observatory_position = me.observatory(observatory)
      sm.setconfig(telescopename = 'e-MERLIN',
                   x = emerlin_xx,
                   y = emerlin_yy,
                   z = emerlin_zz,
                   dishdiameter = emerlin_diam.tolist(),
                   mount = 'alt-az',
                   coordsystem = 'global')
      sm.setfeed(mode='perfect R L')
    elif config.get('observation', 'telescope')=='jvla':
      posvla = me.observatory('vla')
      sm.setconfig(telescopename = 'JVLA',
                   x = vla_xx,
                   y = vla_yy,
                   z = vla_zz,
                   dishdiameter = vla_diam.tolist(),
                   mount = 'alt-az',
                   coordsystem = 'local',
                   referencelocation=posvla)
      sm.setfeed(mode='perfect R L')
    '''
    elif config.get('observation', 'telescope')=='both':
      sm.setconfig(telescopename = 'e-MERLIN+JVLA',
                   x = vla_xx+emerlin_xx,
                   y = vla_yy+emerlin_yy,
                   z = vla_zz+emerlin_zz,
                   dishdiameter = vla_diam.tolist()+emerlin_diam.tolist(),
                   mount = 'alt-az',
                   coordsystem = 'global')
      sm.setfeed(mode='perfect R L')
    '''
    source_dec_casa = config.get('field', 'field_dec').split(':')
    source_dec_casa = source_dec_casa[0]+'d'+source_dec_casa[1]+'m'+source_dec_casa[2]+'s'
    source_dirn = me.direction('J2000', config.get('field', 'field_ra'), source_dec_casa)
    sm.setfield(sourcename = config.get('field', 'name'),
                sourcedirection = source_dirn)
                #sourcedirection = 'J2000 10h30m0.0s +68d0m0.0s')
    
    obs_date = time.strftime('%Y/%m/%d', time.gmtime())
    ref_time = me.epoch('IAT', obs_date)
    sm.settimes(integrationtime = config.get('observation', 't_int')+'s',
                usehourangle = True,
                referencetime = ref_time)

    for i in np.arange(1,n_ifs+1):
      sm.observe(config.get('field', 'name'), 'IF'+str(i),
                 starttime = '0s',
                 stoptime = config.get('observation', 'observation_time')+'s')
  else:
    sm.openfromms(msname)
  sm.predict(imagename=imagename)
  sm.setnoise(mode='simplenoise', simplenoise=config.get('observation', 'noise')+'Jy')
  sm.corrupt()
  sm.done()
  if config.get('imager', 'type')=='nwimager':
    exportuvfits(vis=msname,
               fitsfile=config.get('pipeline', 'project_name')+'.uvfits')
